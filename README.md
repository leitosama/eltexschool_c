# README #

Сборка лаб на языке Си по каталогам  
Основной вариант - **3**

# 9 ЛР - Warcraft

# 10 ЛР - Авиаразведка

# Makefile #

Makefile в корне репозитория нужен для удобного запуска в Sublime text  
make запускает текущую лабу(на момент написания это ЛР7), а make <номер> - лабу по номеру

# Feedback #

[e-mail](mailto:fs.leito@gmail.com)
[vk](https://vk.com/leitosama)