#ifndef __MSG_H__
#define __MSG_H__
//Отдельный header для очереди сообщений
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

const key_t MSGKEY = 20000;
const int MSGPERM = 0600;

struct mymsg
{
	long mtype;
	int mdata;
};
#endif