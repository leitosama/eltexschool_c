//Поиск простых чисел по диапазону
//Работает на очередях SysV IPC
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include "msgq.h"

int nod(int x, int y) { return y ? nod(y, x % y) : x; }

int is_simple(int start, int end, int qid) {
	int flag = 1; int k = 0;

	struct mymsg msg;
	msg.mtype = 1;
	for (int i = start; i <= end; ++i) {
		for (int j = 2; j < i; ++j) {
			if (nod(i, j) != 1) {
				flag = 0; break;
			}
			flag = 1;
		}
		if (flag == 0) {
			continue;
		}
		else {
			++k;
			//Отправляем число
			msg.mdata = i;
			if (msgsnd(qid, &msg, sizeof(msg) - sizeof(long), 0) == -1)
				perror(strerror(errno));
		}
	}
	return k;
}

int compare(const void * x1, const void * x2) { return ( *(int*)x1 - * (int*)x2 ); }



int main(int argc, char *argv[]) {
	//Работа с аргументами командной строки
	int k = 0;
	if (argc != 4) {
		printf("Usage: ./main <нач_число> <кон_число> <диапазон_для_форка>\n");
		return -1;
	}
	int start = atoi(argv[1]);
	int end = atoi(argv[2]);
	int span = atoi(argv[3]);

	//Получение ID очереди
	int qid = msgget(MSGKEY, MSGPERM | IPC_CREAT | IPC_EXCL);
	if (qid == -1) {
		perror(strerror(errno));
		return -2;
	}

	int fork_count = 0;
	while (start < end) {
		if (!fork()) {
			if (end >= start + span)
				return is_simple(start, start + span, qid);
			else
				return is_simple(start, end, qid);
		}
		fork_count++;
		start += span;
	}
	sleep(3);
	// ожидание окончания выполнения всех запущенных процессов
	int status;
	for (int i = 0; i < fork_count; i++) {
		wait(&status);
		if (WIFEXITED(status))
			k += WEXITSTATUS(status);
	}
	// Вывод из очереди
	struct mymsg msg;
	for (int i = 0; i < k; ++i) {
		if (msgrcv(qid, &msg, sizeof(msg) - sizeof(long), 1, 0) == -1) {
			perror(strerror(errno));
		}
		printf("%d\n", msg.mdata);
	}
	//Удаление очереди
	msgctl(qid, IPC_RMID, NULL);
}