//lab3:main.c
//Вариант 3:
// Год издания:Количество страниц:Стоимость:Название книги
// Расположить записи в массиве в алфавитном порядке (по названию)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mcheck.h>
const int MAX_LEN = 256;

struct b
{
	char* name;
	int year, value, pages;
};
typedef struct b book;

int bookcmp(const void *p1, const void* p2) {
	book* b1 = (book*)p1;
	book* b2 = (book*)p2;
	return strcmp(b1->name, b2->name);
}

void read_book(book *b) {
	char format_str[32];
	sprintf(format_str, "%%d:%%d:%%d:%%%d[^\n]s", MAX_LEN);
	b->name = (char*)malloc(sizeof(char) * MAX_LEN);
	scanf(format_str, &(b->year), &(b->pages), &(b->value), b->name);
	char *tmp = realloc(b->name, sizeof(char) * (strlen(b->name) + 1)); // Для \0
	if (tmp != NULL) b->name = tmp;
}

void free_array(book* b, int count) {
	for (int i = 0; i < count; ++i) {
		free(b[i].name);
	}
	free(b);
}

int main(int argc, char const *argv[]) {
	mtrace();
	int count;
	//printf("Количество книг:");
	scanf("%d", &count);
	book *arr = (book*)malloc(sizeof(book) * count);
	//printf("Введите книги в формате\nГод:к-во_стр:цена(руб):название\n");
	for (int i = 0; i < count; ++i) {
		read_book(&arr[i]);
	}
	qsort(arr, count, sizeof(book), bookcmp);
	printf("Год:кол-во_стр:цена(руб):название\n");
	for (int i = 0; i < count; ++i) {
		printf("%d:%d:%d:%s\n", arr[i].year, arr[i].pages, arr[i].value, arr[i].name);
	}
	free_array(arr, count);
	return 0;
}