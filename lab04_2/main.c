//lab4_2: main.c
//Вариант 3:
// Заменить цифры на пробелы
// Параметры командной строки:
// 1. Имя входного файла
// 2. Количество замен
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
const int MAX_LEN = 256;

void nums_to_spaces(FILE* fin, FILE* fout, int count) {
	int ch; int k = 0;
	while ((ch = fgetc(fin)) != EOF) {
		if (ch >= '0' && ch <= '9' && k < count) {
			++k;
			fputc(' ', fout);
		}
		else
			fputc(ch, fout);
	}
}

int main(int argc, char const *argv[])
{
	if (argc != 3) {
		printf("Usage: lab4_2 <file.in> <number>\n");
		return -1;
	}
	int count = atoi(argv[2]);
	char* name_out = (char*) malloc(sizeof(char) * strlen(argv[1]) + 2);
	strncpy(name_out, argv[1], strlen(argv[1]) - 3);
	strcat(name_out, ".out\0");
	FILE *fin = fopen(argv[1], "r");
	FILE *fout = fopen(name_out, "w");
	if (fin == NULL) {
		printf("Не открывается %s\n", argv[1]);
		return -1;
	}
	if (fout == NULL) {
		printf("Не открывается %s\n", name_out);
		return -1;
	}
	nums_to_spaces(fin, fout, count);
	fclose(fin);
	fclose(fout);
	free(name_out);
	return 0;
}