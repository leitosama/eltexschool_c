#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include "shm.h"

const int SLEEPTIME = 2;

int gold_mine(int semid, int shmid) {
	int result = 0; int took; //Для золота
	struct sembuf lock_res = {0, -1, 0};
	struct sembuf unlock_res = {0, 1, 0};
	int *addr = shmat(shmid, NULL, 0);
	while (*addr > 0) {
		sleep(rand() % SLEEPTIME); //Бежит до шахты
		semop(semid, &lock_res, 1);
		sleep(rand() % SLEEPTIME); //Копает
		if (*addr <= 0) {
			/* Если шахта уже пуста*/
			semop(semid, &unlock_res, 1);
			break;
		}
		else {
			if (*addr < 100) //Если <100 забираем остатки
				took = *addr;
			else if (*addr < 200) // Если <200, но >100
				took = rand() % ((*addr) - 100) + 100;
			else
				took = rand() % 100 + 100; // От 100 до 199 голды забирают
		}
		(*addr) -= took;

		semop(semid, &unlock_res, 1);
		result += took;
		sleep(rand() % SLEEPTIME); //Бежит обратно
	}
	shmdt(addr);
	return result;
}

int main(int argc, char const *argv[])
{
	//Работа с аргументами командной строки
	if (argc < 3) {
		fprintf(stderr,"Usage:./main gold workers ");
		return -1;
	}
	srand(getpid());
	int gold = atoi(argv[1]);
	int workers = atoi(argv[2]);

	//IPC
	key_t key = ftok(PATHNAME, PROJ_ID); //Получение ключа
	int semid = semget(key, 1, PERM | IPC_CREAT | IPC_EXCL ); //ID семафора
	if (-1 == semid) {
		fprintf(stderr, "Всё плохо с семафорами\n");
		return -1;
	}
	int shmid = shmget(key, sizeof(int), PERM | IPC_CREAT | IPC_EXCL); //ID shared memory
	if (-1 == shmid) {
		fprintf(stderr, "Всё плохо с shared memory\n");
		return -1;
	}
	int *addr = shmat(shmid, NULL, 0); *addr = gold; shmdt(addr); // Инициализация shm
	semctl(semid, 0, SETVAL, 1); //Инициализация семафора

	//Pipes
	int fd[2];
	if (pipe(fd) < 0) {
		fprintf(stderr,"Всё плохо с pipe\n");
		return -1;
	}

	pid_t pid[workers]; int result;
	for (int i = 0; i < workers; ++i) {
		pid[i] = fork();
		if (-1 == pid[i]) {
			fprintf(stderr,"Всё плохо с fork\n");
			return -1;
		} else if (0 == pid[i]) {
			close(fd[0]);
			sleep(rand() % 3);
			result = gold_mine(semid, shmid);
			write(fd[1], &result, sizeof(result));
			return 0;
		}
	}
	close(fd[1]);
	sleep(1);
	// ожидание окончания выполнения всех запущенных процессов
	for (int i = 0; i < workers; i++) {
		if (waitpid(pid[i], NULL, 0) == -1)
			fprintf(stderr, "Всё плохо c waitpid\n");
		read(fd[0], &result, sizeof(result));
		printf("worker #%d=%d gold\n", i, result);
		//>8 бит, так не работает
		// if (WIFEXITED(status)){
		// 	printf("worker #%d=%d gold\n",i,WEXITSTATUS(status));
		// 	k+=WEXITSTATUS(status);
		// }
	}
	//Удаляем shm и семафорами
	semctl(semid, 0, IPC_RMID);
	shmctl(shmid, IPC_RMID, NULL);
	return 0;
}