#ifndef __SHM_H__
#define __SHM_H__
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <errno.h>
#include <sys/sem.h>

const int PROJ_ID = 42;
const char* PATHNAME = "/tmp/foo";
const int PERM = 0600;

#endif

// union semun {
// 	int val;   /* значение для SETVAL */
// 	struct semid_ds *buf; /*буферы для IPC_STAT, IPC_SET */
// 	unsigned short *array; /* массивы для GETALL, SETALL */
// 	struct seminfo *__buf; /* буфер для IPC_INFO */
// };