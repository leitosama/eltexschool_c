#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int nod(int x, int y) {
	return y ? nod(y, x % y) : x;
}

void is_simple(int start, int end) {
	int flag = 1;
	for (int i = start; i <= end; ++i) {
		for (int j = 2; j < i; ++j) {
			if (nod(i, j) != 1) {
				flag = 0; break;
			}
			flag = 1;
		}
		if (flag == 0) {
			continue;
		}
		else {
			printf("%d\n", i);
		}
	}
}


int main(int argc, char *argv[]) {
	//Работа с аргументами командной строки
	if (argc != 4) {
		printf("Usage: ./main <нач_число> <кон_число> <диапазон_для_форка>\n");
		return -1;
	}
	int start = atoi(argv[1]);
	int end = atoi(argv[2]);
	int span = atoi(argv[3]);

	int fork_count = 0; // Количество форков
	while (start < end) {
		if (!fork()) {
			if (end >= start + span)
				is_simple(start, start + span);
			else
				is_simple(start, end);
			return 0;
		}
		fork_count++;
		start += span;
	}
	sleep(1);
	// ожидание окончания выполнения всех запущенных процессов
	int status;
	for (int i = 0; i < fork_count; ++i) {
		wait(&status);
	}
	return 0;
}