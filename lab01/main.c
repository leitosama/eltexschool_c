#include <stdio.h>
#include <unistd.h>
#include "functions.h"


void func() {
	printf("Hello, im func!\n");
	from_h();
}


int main(int argc, char **argv) {
	printf("Hello, World!\n");
	func();
	from_h();
	sleep(5);
	return 0;
}
