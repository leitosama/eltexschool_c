//Умножение матриц:сервер
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

const int PORT=5000;

typedef struct{
	int linenum;
	int result;
} w_res;

void printerr(const char* str){
	fprintf(stderr, "Error on:%s;errno=%d\n",str,errno);
}

//Отправка
ssize_t mysend(w_res *tosend){
	int sock;
	if ((sock=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP))<0){
		printerr("socket()");
		return -1;
	}
	struct sockaddr_in serv_addr;
	memset(&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_addr.s_addr=htonl(INADDR_LOOPBACK);
	serv_addr.sin_port=htons(PORT);
	serv_addr.sin_family=AF_INET;
	if ((connect(sock,(const struct sockaddr*) &serv_addr,sizeof(serv_addr)))<0){
		printerr("connect()");
		return -1;
	}
	ssize_t result=send(sock,tosend,sizeof(w_res),0);
	return result;
}

int mycmp(const void* v1,const void* v2){
	w_res* s1=(w_res*) v1;
	w_res* s2=(w_res*) v2;
	return s1->linenum-s2->linenum;
}

void w_fork(int linenum,int n,int* matrix,int* vector){
	w_res tosend={linenum,0};
	for (int i=0;i<n;++i){
		tosend.result+=vector[i]*matrix[linenum*n+i];
	}
	return;
}

int create_sock(int server_port);

int main(int argc, char const *argv[]){
	if (argc< 2){
		fprintf(stderr, "Usage: %s <matrix.in> <vector.in> >file.out\n", argv[0]);
		return -1;
	}
	int m,n;
	FILE *fin=fopen(argv[1],"r");
	fscanf(fin,"%d %d",&m,&n);

	int *matrix=(int *) malloc(sizeof(int)*m*n);
	int *vector=(int *) malloc(sizeof(int)*n);
	for (int i=0;i<m*n;++i)
		fscanf(fin,"%d",&(matrix[i]));
	for (int i=0;i<n;++i)
		fscanf(fin,"%d",&(vector[i]));
	fclose(fin);
	
	pid_t child;
	for (int i=0;i<m;++i){
		child=fork();
		if (-1 == child){
			printerr("fork()");
			return -1;
		} else if (0 == child){
			sleep(2);
			w_fork(i,n,matrix,vector);
			return 0;
		}
	}

	int s_sock=create_sock(PORT),cl_sock;
	if (s_sock<0){
		printerr("create_sock()");
		return -1;
	}
	//Получение результатов
	w_res *results=(w_res*) malloc(sizeof(w_res)*m);
	for (int i=0;i<m;++i){
		cl_sock=accept(s_sock,NULL,NULL);
		recv(cl_sock,&(results[i]),sizeof(w_res),0);
		close(cl_sock);
	}
	///Обработка результатов
	qsort(results,m,sizeof(w_res*),mycmp);
	for (int i=0;i<m;++i)
		printf("%d\n",results[i].result );
	return 0;
}

int create_sock(int server_port){
	int sock;
	if ((sock=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP))<0){
		printerr("socket()");
		return -1;
	}

	struct sockaddr_in serv_addr;memset(&serv_addr,0,sizeof(struct sockaddr_in));
	serv_addr.sin_addr.s_addr=htonl(INADDR_LOOPBACK);
	serv_addr.sin_port=htons(server_port);
	serv_addr.sin_family=AF_INET;
	if (bind(sock,(const struct sockaddr *)&serv_addr,sizeof(serv_addr))<0){
		printerr("bind()");
		return -1;
	}
	listen(sock,5);
	return sock;
}