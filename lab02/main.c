//lab2:main.c
//Вариант 3:
// Расположить строки в алфавитном порядке
// Входные параметры:
// 1. Массив
// 2. Размерность массива
// Выходные параметры:
// 1. Количество перестановок
// 2. Первая буква первой строки
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mcheck.h>
const int MAX_LEN = 256; // Ограничение в длине строки

//Ввод строки
int inp_str(char* string, int maxlen) {
	// Форматирование строки для ввода
	char format_str[10];
	sprintf(format_str, "%%%d[^\n]s", maxlen);

	scanf(format_str, string);
	getchar(); //Магия \n для scanf
	return strlen(string);
}

//Вывод строки
void out_str(char* string, int length, int number) {
	printf("%d(%d): %s\n", number, length, string );
	return;
}

//Сортировка
int sort(char** arr, int num) {
	int k = 0; char *tmp;
	for (int i = 0; i < num - 1; ++i)
		for (int j = i + 1; j < num; ++j)
			if (strcmp(arr[i], arr[j]) > 0) {
				++k;
				tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;
			}
	return k;
}

int main(int argc, char **argv) {
	mtrace();
	char* tmp;
	int count; // Количество строк, вводимое пользователем
	int len; // Для длин строк
	//printf("Количество строк:");
	scanf("%d", &count);
	getchar(); //Магия незначищих символов scanf
	char **strings = (char**) malloc(sizeof(char*)*count);
	//printf("Строки:\n");
	for (int i = 0; i < count; ++i) {
		strings[i] = (char*) malloc(sizeof(char) * MAX_LEN);
		len = inp_str(strings[i], MAX_LEN - 1); // Магическая единица созданная специально для \0
		tmp = (char*) realloc(strings[i], sizeof(char) * (len + 1)); //len+1 для \0
		if (tmp != NULL) strings[i] = tmp;
	}
	tmp = NULL;
	printf("Количество перестановок=%d\n", sort(strings, count));
	printf("1ая буква 1ой строки:%c\n", strings[0][0]);
	for (int i = 0; i < count; ++i) {
		out_str(strings[i], strlen(strings[i]), i);
		free(strings[i]);
	}
	free(strings);
	return 0;
}