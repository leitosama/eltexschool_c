#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

struct m
{
	int x_max;
	int y_max;
	int *targets;
};

typedef struct m map;

void *pthread_func(void* arg) {
	map* m = (map*)arg; //Получаем карту
	int x = rand() % m->x_max, y = rand() % m->y_max;

	int *result = (int*) malloc(sizeof(int));
	if ((x>y && x>m->y_max-y-1) || (m->x_max-1-x>y && m->x_max-1-x>m->y_max-y-1)){
		if (2*x<=m->x_max-1){
			for (;x<m->x_max;++x) if (m->targets[x+m->y_max*y]==1) ++(*result);
		}
		else{
			for (;x>-1;--x) if (m->targets[x+m->y_max*y]==1) ++(*result);
		}
	}
	else
		if (2*y<=m->y_max-1){
			for (;y<m->y_max;++y) if (m->targets[x*m->x_max+y]==1) ++(*result);				
		}
		else{
			for (;y>-1;--y) if (m->targets[x*m->x_max+y]==1) ++(*result);	
		}
	return result;
}


int main(int argc, char const *argv[])
{
	if (argc < 4) {
		fprintf(stderr, "Usage:./main <x_max> <y_max> <plane_nums>\n");
		return -1;
	}
	srand(getpid());
	//Карта
	map m = { atoi(argv[1]), atoi(argv[2]), NULL};
	m.targets = (int*)malloc(sizeof(int) * m.x_max * m.y_max);
	if (m.targets==NULL){
		fprintf(stderr, "Не выделяется память для \"целей\"\n" );
		return -1;
	}
	for (int i = 0; i < m.x_max * m.y_max; ++i) 
		m.targets[i] = rand() % 4; //от 0 до 3, целью является 1!

	//Работа с потоками
	int planes = atoi(argv[3]);int status;
	pthread_t *threads = (pthread_t*) malloc(sizeof(pthread_t) * planes);
	for (int i = 0; i < planes; ++i){
		status=pthread_create(&threads[i], NULL, pthread_func, &m);
		if (status!=0){
			fprintf(stderr, "Не создается поток %d\n",i );
			return -1;
		}
	}

	//Результат
	int **result=(int**)malloc(sizeof(int*)*planes);
	if (result==NULL){
		fprintf(stderr, "Не выделяется память для результатов\n" );
		return -1;
	}
	for (int i=0;i<planes;++i){
		status=pthread_join(threads[i],(void**)&result[i]);
		if (status!=0)
			fprintf(stderr, "Поток %d не присоединяется\n",i );
		printf("i=%d;result=%d\n",i,*((int*)result[i]) );
	}
	// Освобождение памяти
	for (int i=0;i<planes;++i)
		free(result[i]);
	free(result);
	free(m.targets);
	free(threads);
	return 0;
}
