//Поиск простых чисел по диапазону
//Не использовал именновые каналы, смысла в них нет;
//Дочерние процессы закидывают в pipe значения и возвращают количество найденных чисел
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <math.h>

int nod(int x, int y) { return y ? nod(y, x % y) : x; }

int is_simple(int start, int end, int *fd) {
	int flag = 1; int k = 0;
	for (int i = start; i <= end; ++i) {
		for (int j = 2; j < i; ++j) {
			if (nod(i, j) != 1) {
				flag = 0; break;
			}
			flag = 1;
		}
		if (flag == 0) {
			continue;
		}
		else {
			++k;
			write(fd[1], &i, sizeof(i));
		}
	}
	close(fd[1]);
	return k;
}

int compare(const void * x1, const void * x2) { return ( *(int*)x1 - * (int*)x2 ); }

int main(int argc, char *argv[]) {
	//Работа с аргументами командной строки
	int k = 0;
	if (argc != 4) {
		printf("Usage: ./main <нач_число> <кон_число> <диапазон_для_форка>\n");
		return -1;
	}
	int start = atoi(argv[1]);
	int end = atoi(argv[2]);
	int span = atoi(argv[3]);

	int fd[2];
	int fork_count = 0;
	if (pipe(fd) < 0) {
		perror("Всё плохо");
		return -1;
	}
	while (start < end) {
		if (!fork()) {
			close(fd[0]);
			if (end >= start + span)
				return is_simple(start, start + span, fd);
			else
				return is_simple(start, end, fd);
		}
		fork_count++;
		start += span;
	}
	close(fd[1]);
	sleep(1);
	// ожидание окончания выполнения всех запущенных процессов
	int status;
	for (int i = 0; i < fork_count; i++) {
		wait(&status);
		//Не будет работать для >8 бит
		if (WIFEXITED(status))
			k += WEXITSTATUS(status);
	}
	// Вывод из pip'а; Что-то можно с этим делать
	int buf[k];
	read(fd[0], buf, sizeof(int)*k);
	//Ну допустим сортируем
	qsort(buf, k, sizeof(int), compare);
	//Выводим
	for (int i = 0; i < k; ++i)
		printf("%d\n", buf[i]);
	close(fd[0]);
	return 0;
}