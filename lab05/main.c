#include <stdio.h>
#include <dlfcn.h>

int main(int argc, char const *argv[])
{
	int value = 4;
	void *external_lib = dlopen("./libf.so", RTLD_LAZY);
	if (!external_lib) {
		fprintf(stderr, "dlopen error:%s\n", dlerror());
		return -1;
	}
	int (*squarefunc)(int);
	double (*rootfunc)(int, int);
	squarefunc = dlsym(external_lib, "square");
	rootfunc = dlsym(external_lib, "root");
	printf("%d^2=%d\n", value, (*squarefunc)(value));
	printf("%d^1/%d=%f\n", value, 2, (*rootfunc)(value, 2));
	dlclose(external_lib);
	return 0;
}