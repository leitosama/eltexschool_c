//lab4_2: main.c
//Вариант 3:
//Исключить строки, начинающиеся с заданного слова
//Параметры командной строки:
//	1. Имя входного файла
//	2. Заданное слово
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
const int MAX_LEN = 256;

//Начинается ли строка со слова?
int check_word(const char* str, const char* word) {
	int i = 0;
	while (str[i] != ' ' && str[i] != '\n' && str[i] != '\0') {
		if (str[i] == word[i]) {
			++i;
			continue;
		}
		else return 0;
	}
	return 1;
}

void excluse(FILE* fin, FILE* fout, const char* word) {
	char tmp[MAX_LEN];
	while (!feof(fin)) {
		fgets(tmp, MAX_LEN, fin);
		if (!check_word(tmp, word))
			fputs(tmp, fout);
	}
}

int main(int argc, char const *argv[])
{
	if (argc != 3) {
		printf("Usage: lab4 <file.in> <word>\n");
		return -1;
	}
	char* name_out = (char*) malloc(sizeof(char) * strlen(argv[1]) + 2); // На 1 больше и \0
	strncpy(name_out, argv[1], strlen(argv[1]) - 3); // Копируем без .in
	strcat(name_out, ".out\0");
	FILE *fin = fopen(argv[1], "r");
	FILE *fout = fopen(name_out, "w");
	if (fin == NULL) {
		printf("Не открывается %s\n", argv[1]);
		return -1;
	}
	if (fout == NULL) {
		printf("Не открывается %s\n", name_out);
		return -1;
	}
	excluse(fin, fout, argv[2]);
	fclose(fin);
	fclose(fout);
	free(name_out);
	return 0;
}